<?php
require_once("animal.php");
class Frog extends Animal
{
    public function jump()
    {
        echo "Name: " . $this->name . "<br>";
        echo "leg: " . $this->legs . "<br>";
        echo "cold blooded: " . $this->cold_blooded . "<br>";
        echo "Jump: Hop Hop<br>";
    }
}
