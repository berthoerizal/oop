<?php
require_once("animal.php");
class Ape extends Animal
{
    public function yell()
    {
        $this->legs = 2;
        echo "Name: " . $this->name . "<br>";
        echo "leg: " . $this->legs . "<br>";
        echo "cold blooded: " . $this->cold_blooded . "<br>";
        echo "Yell: Auooo<br>";
    }
}
